# Lab 3 Challenge Task: "Persistence as a policy!"
### Disclaimer: COMP2100 students are only required to do questions Q1 and Q2 to get full marks (you may attempt the rest if you want to learn more about the subject). COMP6442 students _must_ answer all of the questions correctly (Q1- Q3). 

### Read through the assignment first, _do not copy/paste the code you see here_. You can find a link to the full starter project [here](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab3/MarkingTool.zip). 


## Motivation
As you've probably gathered, almost every app persists data at some point (Notepads, Spreadsheets, Messaging Apps, ...). Thus, __persistence is one of the most crucial components in any application__. However, this also makes it one of the __most tedious and bug-prone components to develop__ and can easily _distract you_ from developing more serious core-related features.  

The purpose of this task is to show you that we can leave the development of a complicated persistence engine for last. This means you can develop and test your entire app without __depending__ on the ugly parts of persistence!

## The app: "Marking Tool"
The app we're gonna develop is a marking tool that helps me (your tutor) save and update all your marks. You can add/update a student's mark by entering their ID and their mark and clicking "ADD/UPDATE":

<img src="https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/204537f9fe049773aa4646841d53c232facead44/lab3/screen1.png" alt="screenshot1" height= "700px"/>


And you can retrieve a student's mark later by entering their ID and clicking "GET GRADE":

<img src="https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/204537f9fe049773aa4646841d53c232facead44/lab3/screen2.png" alt="screenshot2" height= "700px"/>


### DISCLAIMER: Don't freak out! The only component you need to develop is the __persistence engine__. Everything else has been implemented for you.

## Persistence as a policy
When modularising an application, it's important to separate a module's __policy__ (_what_ it's supposed to do) from its mechanism (_how_ it's carries out the policy). We can specify our persistence engine's __policy__  by defining the `PersistenceEngine` interface:
```java
public interface PersistenceEngine {
    /**
     * Finds a student in persisted data by their ID
     */
    Student findStudentById(final int id);

    /**
     *
     * Persists a student
     */
    void persistStudent(Student student);
}
```

You might think `findStudentById()` should be in the __core__ module of our app (and you'd be right to an extend). However, if you're persisting huge quantities of data, it's normal to carry out these queries in the persistence module for performance reasons (this will be made clearer as we go along).


## Implementing a dummy/stub persitence engine
We are now ready to provide our first __persistence mechanism__. Since we want to see if our app _actually works_ we don't want to worry about complex persistence details now. So we'll implement a dummy persistence engine that saves data into memory __but doesn't actually persist anything__. We can simply store/retrieve our `Student`s in/from a `HashMap` as you can see in `MemoryPersistenceEngine`:
```java
public class MemoryPersistenceEngine implements PersistenceEngine {
    // Map of students indexed by student ID
    private final Map<Integer, Student> studentMap = new HashMap<>();

    @Override
    public Student findStudentById(final int id) {
        // YOUR IMPLEMENTATION HERE
    }

    @Override
    public void persistStudent(Student student) {
        // YOUR IMPLEMENTATION HERE
    }
}
```

__Q1 [1 mark for COMP2100, 0.5 mark for COMP6442]__: Finish implementing `findStudentById()` and `persistStudent()`. These methods should respectively get/put a student from/in `studentMap`.

Now try running the app! Hopefully, you'll see that the app is doing everything it's supposed to __without writing a single line of horrible IO code__!

## Implementing a CSV persistence engine
Now that we've seen the app is working as it's supposed to, we can move on to the actual persistence engine. We will be storing our files in the CSV format where __the first column will indicate the ID (an `int`)__ of each student we're saving and __the second column will indicate their grade (a `double`)__. If we'd saved the marks of two students the CSV file would look like this:
```
5786025,56.0
587290,92.3
```

Note that we want our persistence engine to be highly __memory efficient__ (we don't want our students loaded all at once). To do this we will
* _iterate through the file_ whenever we want to retrieve a grade.
* and _append a new row_ whenever we add or update a student's grade.

The best way to do this is to use a `Scanner` and `PrintWriter` for finding and persisting a student respectively. We have started the process in `AndroidFilePersistenceEngine`:
```java
public class AndroidFilePersistenceEngine implements PersistenceEngine {
    private final String fileName;
    private final Context context;

    public AndroidFilePersistenceEngine(String fileName, Context context) {
        this.fileName = fileName;
        this.context = context;
    }

    @Override
    public Student findStudentById(int id) {
        Student student = null;
        try (final Scanner scanner = new Scanner(context.openFileInput(fileName))){
            // Scan each line of the CSV file
            while (scanner.hasNextLine()) {
                final String line = scanner.nextLine();
                final Scanner lineScanner = new Scanner(line);
                // YOUR IMPLEMENTATION HERE!
            }
        } catch (FileNotFoundException ignored) {
        }
        return student;
    }

    @Override
    public void persistStudent(Student student) {
        try (final PrintWriter writer = new PrintWriter(context.openFileOutput(fileName, Context.MODE_APPEND))){
        // YOUR IMPLEMENTATION HERE!
        } catch (FileNotFoundException e) {
        }
    }
}
```
__Q2 [1 mark for COMP2100, 0.5 mark for COMP6442]__: Finish implementing `findStudentById()` and `persistStudent()` in `AndroidFilePersistenceEngine.java`. You can find hints on how to do this within the actual downloaded code. To check if your implementation works go to `MainActivity.java` and in `onResume` uncomment the following line:

```java
core = new MarkingToolCore(new AndroidFilePersistenceEngine("data.csv", this));
```

You should re-run the app and confirm if the data is actually persist it (exit it and re-run it within the emulator).

__Conclusion__: Note that at this point we __maintained the policy__ (the interface) and simply __changed the mechanism__ (the implementation)! This is the great advantage of separating a module's __policy__ from its __mechanism(s)__. The app __doesn't know or care__ about _how we do persistence_ as long as the implementation is carried out correctly. This means we can bombard our app with tests and be confident that they'll still hold once we change the mechanism! Any errors occuring from thereon can be immediately isolated to the new mechanism.

### DISCLAIMER: The following tasks are for COMP6442 students only (you can still attempt them if you want to learn someting extra).

## Building a faster persistence engine!
By now you've probably noticed the pros and cons for each engine:

`AndroidFilePersistenceEngine` : 
* __Pro__: stores stuff very quickly (it appends new records)! 
* __Con__: searching for a student is _very slow_ if we have lots of records stored. 

`MemoryPersistenceEngine`:
* __Pro__: searching is _lightning-fast_ because everything is in memory! 
* __Con__: Doesn't store anything on disk (doesn't actually persist anything).

Is there a way to combine them both? Fortunately, yes! We can use `MemoryPersistenceEngine` as a _cache_ (temporary storage) for fast searches and `AndroidFilePersistenceEngine` to actually store/update the data.

To do this we'll be using `CachedPersistenceEngine` which combines both efforts:

```java
public class CachedPersistenceEngine implements PersistenceEngine {
    private final PersistenceEngine heavyEngine;
    private final MemoryPersistenceEngine cacheEngine;

    public CachedPersistenceEngine(PersistenceEngine heavyEngine) {
        this.heavyEngine = heavyEngine;
        this.cacheEngine = new MemoryPersistenceEngine();
    }

    @Override
    public Student findStudentById(int id) {
    // YOUR IMPLEMENTATION HERE!
    }

    @Override
    public void persistStudent(Student student) {
    // YOUR IMPLEMENTATION HERE!
    }
}
```
__Our goal__: Essentially, `heavyEngine` (the slower engine) will handle persisting the data while `cacheEngine` will handle our lightning-fast searches. 

Whenever we search for a student we'll try `cacheEngine` first. If we can't find a student there, we'll defer to the slower `heavyEngine` and update `cacheEngine` if a student is found (so that same record is retrieved much faster next time)! 

Persistence should be primarily relayed to `heavyEngine` updating `cacheEngine` only when it has a previous record of the data we're trying to persist. 

__Q3 [1 mark for COMP6442]__: Finish implementing `findStudentById()` and `persistStudent()` in `CachedPersistenceEngine.java`. You can find hints on how to do this within the actual downloaded code. To check if your implementation works go to `MainActivity.java` and in `onResume` uncomment the following line:
```java
core = new MarkingToolCore(new CachedPersistenceEngine(new AndroidFilePersistenceEngine("data.csv", this)));
```
