package jason.comp2100.markingtool.persistence;

import jason.comp2100.markingtool.core.Student;

import java.util.HashMap;
import java.util.Map;

public class MemoryPersistenceEngine implements PersistenceEngine {
    // Map of students indexed student ID
    private final Map<Integer, Student> studentMap = new HashMap<>();

    @Override
    public Student findStudentById(final int id) {
        // YOUR Q1 CODE HERE
        // you should return the student mapped to the supplied ID in studentMap.
        return null;
    }

    @Override
    public void persistStudent(Student student) {
        // YOUR Q1 CODE HERE
        // You should add the student to studentMap. You should the student's ID as the key.
    }
}
