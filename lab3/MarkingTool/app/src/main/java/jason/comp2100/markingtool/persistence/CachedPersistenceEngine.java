package jason.comp2100.markingtool.persistence;

import jason.comp2100.markingtool.core.Student;

/**
 * Created by jason on 28/03/17.
 */
public class CachedPersistenceEngine implements PersistenceEngine {
    private final PersistenceEngine heavyEngine;
    private final MemoryPersistenceEngine cacheEngine;

    public CachedPersistenceEngine(PersistenceEngine heavyEngine) {
        this.heavyEngine = heavyEngine;
        this.cacheEngine = new MemoryPersistenceEngine();
    }

    @Override
    public Student findStudentById(int id) {
        // YOUR Q3 CODE HERE! (COMP6442 students only!)
        // You should try to find the student cacheEngine first. If you can't then you should defer to heavyEngine.
        return null;
    }

    @Override
    public void persistStudent(Student student) {
        // YOUR Q3 CODE HERE! (COMP6442 students only!)

        // You should persist the student in cacheEngine ONLY IF the student is already cached (i.e, found in cacheEngine).
        // After that you should persist the student to heavyEngine.
    }
}
