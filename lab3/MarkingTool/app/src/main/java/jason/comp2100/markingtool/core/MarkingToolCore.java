package jason.comp2100.markingtool.core;

import jason.comp2100.markingtool.persistence.PersistenceEngine;

public class MarkingToolCore {
    private final PersistenceEngine persistenceEngine;

    /**
     *     Create new Core representation that saves/loads data from the specified persistence engine
     */
    public MarkingToolCore(PersistenceEngine persistenceEngine) {
        this.persistenceEngine = persistenceEngine;
    }

    /**
     * Adds/updates a student record.
     */
    public void addNewStudent(final Student student) {
        persistenceEngine.persistStudent(student);
    }

    /**
     * Finds a student by their ID.
     */
    public Student findStudent(final int id) {
        return persistenceEngine.findStudentById(id);
    }
}
