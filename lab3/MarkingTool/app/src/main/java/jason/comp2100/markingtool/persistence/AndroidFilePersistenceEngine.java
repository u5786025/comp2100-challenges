package jason.comp2100.markingtool.persistence;

import android.content.Context;
import jason.comp2100.markingtool.core.Student;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class AndroidFilePersistenceEngine implements PersistenceEngine {
    private final String fileName;
    private final Context context;

    public AndroidFilePersistenceEngine(String fileName, Context context) {
        this.fileName = fileName;
        this.context = context;
    }

    @Override
    public Student findStudentById(int id) {
        Student student = null;
        try (final Scanner scanner = new Scanner(context.openFileInput(fileName))){
            // Scan each line of the CSV file
            while (scanner.hasNextLine()) {
                final String line = scanner.nextLine();
                final Scanner lineScanner = new Scanner(line);

                // YOUR Q2 CODE HERE

                // You should:
                // 1 - set lineScanner's delimiter to ',' (comma)
                // 2 - extract the next student's ID (an integer) and their grade (a double).
                // 3 - If the student's ID matches the one you're looking for, update the student variable.

                // WARNING:
                // *DO NOT EXIT/RETURN* EARLY FROM THIS LOOP (i.e, as soon as you find a match) as the file we don't delete
                // outdated records!
            }
        } catch (FileNotFoundException ignored) {
        }
        return student;
    }

    @Override
    public void persistStudent(Student student) {
        try (final PrintWriter writer = new PrintWriter(context.openFileOutput(fileName, Context.MODE_APPEND))){
            // YOUR Q2 CODE HERE
            // You should use one of the writer's print*() functions to write a row representing a student.
            // A student with id 123 and grade 52.5 would be printed as:
            // 123,52.5
        } catch (FileNotFoundException e) {
        }
    }
}
