package jason.comp2100.markingtool.core;

public class Student {
    private int id;
    private double grade;

    public Student(int id, double grade) {
        this.id = id;
        setGrade(grade);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getGrade() {
        return grade;
    }

    // Grades clamped to 0 - 100;
    public void setGrade(double grade) {
        this.grade = clamp(grade, 0, 100);
    }

    private static double clamp(double value, double low, double high) {
        return value < low ? low : (value > high ? high : value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return id == student.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
