package jason.comp2100.markingtool.persistence;

import jason.comp2100.markingtool.core.Student;

public interface PersistenceEngine {
    /**
     * Finds a student in persisted data by their ID
     */
    Student findStudentById(final int id);

    /**
     *
     * Persists a student
     */
    void persistStudent(Student student);
}
