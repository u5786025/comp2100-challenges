package jason.comp2100.markingtool.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import jason.comp2100.markingtool.R;
import jason.comp2100.markingtool.core.MarkingToolCore;
import jason.comp2100.markingtool.core.Student;
import jason.comp2100.markingtool.persistence.AndroidFilePersistenceEngine;
import jason.comp2100.markingtool.persistence.CachedPersistenceEngine;
import jason.comp2100.markingtool.persistence.MemoryPersistenceEngine;

public class MainActivity extends AppCompatActivity {
    private MarkingToolCore core;
    private EditText studentIdEdit, studentGradeEdit;
    private EditText foundIdEdit, foundGradeEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentIdEdit = (EditText) findViewById(R.id.studentIdEdit);
        studentGradeEdit = (EditText) findViewById(R.id.gradeEdit);

        foundIdEdit = (EditText) findViewById(R.id.foundStudentEdit);
        foundGradeEdit = (EditText) findViewById(R.id.foundGradeEdit);
    }

    @Override
    protected void onResume() {
        // Set up core
        core = new MarkingToolCore(new MemoryPersistenceEngine());

        // Uncomment for Q2
        // core = new MarkingToolCore(new AndroidFilePersistenceEngine("data.csv", this));

        // Uncomment for Q3 (COMP6442 students only!)
        // core = new MarkingToolCore(new CachedPersistenceEngine(new AndroidFilePersistenceEngine("data.csv", this)));
        super.onResume();
    }

    // Add/Update listener
    public void addStudent(View view) {
        final String id = studentIdEdit.getText().toString().trim();
        final String grade = studentGradeEdit.getText().toString().trim();

        // Can't have empty fields
        if (id.isEmpty() || grade.isEmpty()) {
            Toast.makeText(this, "Can't have any empty fields!", Toast.LENGTH_LONG).show();
            return;
        }

        // Data is OK, send it to the core!
        core.addNewStudent(new Student(Integer.parseInt(id), Double.parseDouble(grade)));
        Toast.makeText(this, "Student grade entered successfully!", Toast.LENGTH_LONG).show();
    }

    // Get Grades listener
    public void getGrade(View view) {
        // Get string representation of ID.
        final String id = foundIdEdit.getText().toString().trim();

        // Check if it's valid input
        if (id.isEmpty()) {
            Toast.makeText(this, "Please fill in the 'Student ID' field", Toast.LENGTH_LONG).show();
            return;
        }

        // It's valid, forward to the core!
        final Student student = core.findStudent(Integer.parseInt(id));
        if (student == null) {
            // Couldn't find student, show it to the user!
            Toast.makeText(this, "Couldn't find any student with that ID.", Toast.LENGTH_LONG).show();
            foundGradeEdit.setText("");
            return;
        }

        // Found student record, show it to the user!
        foundGradeEdit.setText(String.valueOf(student.getGrade()));
    }
}
