package com.example.jason.numberadder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jason on 15/03/17.
 */
public class AdderCore {
    private int firstNumber, secondNumber, result;

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getResult() {
        return result;
    }

    public void addNumbers() {
        // Q1: Implement the addNumbers() method [1 mark for COMP2100, 0.5 mark for COMP6442]
        // you should update the result with the sum of the first and second numbers

        // YOUR CODE HERE!

        // DO NOT REMOVE THE FOLLOWING LINE
        // propagates the new value to throughout the subscribers
        notifySubscribers(subscribers, result);
    }


    // COMP6442 ONLY BEYOND THIS LINE (unless you're curious :-))
    private final List<Subscriber> subscribers = new ArrayList<>();

    public void addSubscriber(final Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void clearSubscribers() {
        subscribers.clear();
    }

    private static void notifySubscribers(List<Subscriber> subscribers, int newValue) {
        // Q3: Finish implementing the notifySubscribers() method [0.25 mark for COMP6442]
        // You should propagate the new value throughout the subscribers.

        // YOUR CODE HERE!
    }
}
