package com.example.jason.numberadder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private AdderCore adderCore;
    private EditText firstNumberEdit, secondNumberEdit;
    private TextView resultText;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Get all the UI components.
        setContentView(R.layout.activity_main);
        firstNumberEdit = (EditText) findViewById(R.id.firstNumberEdit);
        secondNumberEdit = (EditText) findViewById(R.id.secondNumberEdit);
        resultText = (TextView) findViewById(R.id.resultText);
        addButton = (Button) findViewById(R.id.addButton);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        adderCore = new AdderCore();
        // Load UI components based on information provided from the core
        loadUIFromAdderCore(adderCore);

        // Load all component listeners
        loadListeners(adderCore);

        // Add all update-subscribers [COMP6442 only]
        publishSubscribers(adderCore);
        super.onResume();
    }

    private static int stringToInt(final String stringInt) {
        return stringInt.isEmpty() ? 0 : Integer.parseInt(stringInt);
    }

    private void loadListeners(final AdderCore adderCore) {
        // Q2 [1.5 mark for COMP2100, 0.75 mark for COMP6442]: Implement the loadListeners() function. You should add a TextWatcher to each EditText
        // and implement the afterTextChanged() method. Each TextWatcher MUST UPDATE the corresponding field
        // in adderCore.

        // You must also implement the addButton's onClick listener which:
        // 1 - Tells adderCore to compute the result.
        // 2 - Sets resultText's text to the result saved in adderCore (DON'T FORGET TO CONVERT THE RESULTING INT INTO
        // A STRING!).

        // IMPORTANT: If you need to convert a String to an int YOU SHOULD USE the supplied stringToInt() helper function.
    }

    @Override
    protected void onPause() {
        adderCore.clearSubscribers();
        super.onPause();
    }

    private void loadUIFromAdderCore(AdderCore adderCore) {
        firstNumberEdit.setText(String.valueOf(adderCore.getFirstNumber()));
        secondNumberEdit.setText(String.valueOf(adderCore.getSecondNumber()));
        resultText.setText(String.valueOf(adderCore.getResult()));
    }

    // COMP6442 ONLY BEYOND THIS LINE (unless you're curious :-))
    private void publishSubscribers(AdderCore adderCore) {
        // Q4: YOUR CODE HERE!
        // You should add ONE subscriber that updates the result TextView with the most recently calculated result.
    }
}
