package com.example.jason.numberadder;

// COMP6442 ONLY!
// DO NOT CHANGE THIS CODE
public interface Subscriber {
    void onUpdate(int result);
}
