# Lab 2 Challenge Task: "Separating the presentation from the core" [2 marks]
### Disclaimer: COMP2100 students are only required to do questions Q1 and Q2 to get full marks (you may attempt the rest if you want to learn more about the subject). COMP6442 students _must_ answer all of the questions correctly (Q1- Q4). 

### Read through the assignment first, _do not copy/paste the code you see here_. You can find a link to the full starter project at the end of this page. 


## Motivation
One of the cornerstones of good software design is the idea of _modularisation_. Although there are many ways to modularise code, we must take extreme care when developing Graphical User Interface (GUI) applications. One of the most natural and popular ways of moduralising a GUI application is:
* __Separating the presentation (visual) part from the internal logic (the core) of the application__.

Separating the __core__ from the __presentation__ results in far more cleaner, portable, and testable code. In other words it's easier to port, say, an _Android_ game to a _desktop_ game or even a _browser_ game. Testing is also made easier because once the core is fully tested, we can be sure that any new bug we find _must be_ in one of the other non-core modules. 


## The app: _Number Adder_
The app you will be working on is one that simply adds the two numbers given to it:

<img src="https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/17808b180e603b7460035f2c6acec05be5d6b76c/lab2/Screenshot_1489635106.png" alt="screenshot" height= "700px"/>

## Number Adder's core class
The following class is a _partial_ implementation of the Number Adder's core:  
```java
public class AdderCore {
    private int firstNumber, secondNumber, result;

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getResult() {
        return result;
    }

    public void addNumbers() {
        // TO BE IMPLEMENTED
    }
    // ...
```

Notice how the code only contains _pure Java_ without any reference to Android components or functions, thus making it trivial to port to other Java-based platforms. 

__Q1 [0.5 mark for COMP2100, 0.25 mark for COMP6442]__: Implement the addNumbers() method. You should update the result with the sum of the first and second numbers.

## The presentation part.

Now let's take a look at the presentation part:
```java
public class MainActivity extends AppCompatActivity {
    private AdderCore adderCore;
    private EditText firstNumberEdit, secondNumberEdit;
    private TextView resultText;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Get all the UI components.
        setContentView(R.layout.activity_main);
        firstNumberEdit = (EditText) findViewById(R.id.firstNumberEdit);
        secondNumberEdit = (EditText) findViewById(R.id.secondNumberEdit);
        resultText = (TextView) findViewById(R.id.resultText);
        addButton = (Button) findViewById(R.id.addButton);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        adderCore = new AdderCore();
        // Load UI components based on information provided from the core
        loadUIFromAdderCore(adderCore);
        
        // Load all component listeners
        loadListeners(adderCore);
        
        //...
    }
    
    private void loadListeners(final AdderCore adderCore) {
    // YOUR IMPLEMENTATION HERE
    }
```
__Q2 [1.5 mark for COMP2100, 0.75 mark for COMP6442]__: Implement the `loadListeners()` function.
You should add a `TextWatcher` to each `EditText` and implement the `afterTextChanged()` method. Each `TextWatcher` MUST UPDATE the corresponding field in `adderCore`.

You must also implement the compute button's `onClick` listener which:
 * Tells `adderCore` to compute the result.
 * Sets `resultText`'s text to the result saved in `adderCore`.

### DISCLAIMER: The following tasks are for COMP6442 students only (you can still attempt them if you want to learn someting extra).

## Making our core more reactive!

If you implemented Q2 correctly you probably noticed that after telling adderCore to compute the result you had to __tell__ the interface to update the resulting TextView, which can be a bit error prone for more complex projects. An elegant way around this issue is to use __reactive programming__, a programming style that focuses on data flows and propagating changes to that data. In other words, we want our core to somehow __notify the UI that it needs to update the resulting field__. 

One way to do this is by making our _keep track_ of anyone interested in knowing when `result` is updated. Anyone insterested in receiving updates is called a `Subscriber`, defined below:
```java
public interface Subscriber {
    void onUpdate(int result); // A subscriber is notified of a new result via this method. 
}
```

We can keep track of all our subscribers by adding the following field and methods:
```java 
private final List<Subscriber> subscribers
    public void addSubscriber(final Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void clearSubscribers() {
        subscribers.clear();
    }
```

Now to notify our subscribers that the result as been updated we can create a method:
```java
    private static void notifySubscribers(List<Subscriber> subscribers, int newValue) {
        // YOUR IMPLEMENTATION HERE
    }
```

And call it at the end of `addNumbers()` so that the new result is propagated.

__Q3 [0.25 mark for COMP6442]__: Finish implementing the `notifySubscribers()` method. You should propagate the new value throughout the subscribers.

Now that have a reactive subscriber mechanism, we need to tell the UI to __subscribe__ to `adderCore`'s result updates. We do this by calling the following method in `onResume()`:
```
private void publishSubscribers(AdderCore adderCore) {
// YOUR IMPLEMENTATION HERE
}
```

__Q4 [0.75 mark for COMP6442]__: Finish implementing the `publishSubscribers()` method. You should add ONE `Subscriber` that updates the result `TextView` with the most recently calculated result.


## You can download the starter code [here](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab2/NumberAdder.zip).