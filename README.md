# Lab 4 Challenge Task: "Ditching recursion!" [2 marks]
### Disclaimer: You are required to answer all questions correctly to get full marks. 

### Read through the assignment first, _do not copy/paste the code you see here_. You can find a link to the full starter project [here](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab4/lab4.zip). 


## Motivation
By now you've probably learned why Trees are _recursive data structures_, they are normally defined and used recursively. However, recursively iterating through trees is __really bad__ because you have to save the stack frame for each recursive call which can easily result in a stack overflow (this is where stackoverflow.com got its name from). Even if it doesn't result in a stack overflow, it still makes your code run a lot slower. Having said all of that, the following rule should come as no surprise:

 __Stay away from recursion when doing serious programming. If you have a recursive algorithm try your best to de-recurse it__ (i.e, try implementing the same algorithm without using recursion).  

The purpose of this lab is to show you that de-recursing an algorithm can be easier than you'd think if you're familiar with a couple of cool ideas. 

## Introducing. the _Max-Tree_

Today we're gonna be looking at a Max-Tree. The special thing about this tree is that each node is either:
1. a __leaf__ (no children) or 
2. the __maximum of its children__.

The following figure illustrates this definition.

![tree1](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab4/tree1.png "A Max Tree")

The main interface we'll be using will be `MaxTree`:
```java
public interface MaxTree {
    double max(); // returns the node value (i.e. the maximum of its children, or an arbitrary value in case it's a leaf)
    List<MaxTree> getChildren(); // Returns the tree's children in case there are any
    boolean isLeaf(); // is the tree a leaf? call this method to find out!
}
```

The easiest way to go about implementing this data structure would be to calculate the maximum recursively until will reached the leaves. However, as we stated above using a purely recursve solution is slow and would cause the program to crash for deep trees.

Before we move on, we'll need a method that calculates the maximum of all children in a tree (this corresponds to item 2. in the definition above). We'll place this method in the file `MaxTreeUtils.java`:

```java
    public static double max(final List<? extends MaxTree> children) {
    // ...
    }
```
__Q1 [0.5 marks]__: Implement the `max()` helper function specified above (you can find hints in the downloaded code).

You're probably saying to yourself _my implementation looks a bit recursive_ however, since you're deferring childrens' `max()` methods, this method will only be recursive __if the childrens' `max()` method is also recursive__, so don't worry. Onwards towards de-recursion!

## Non-recursive Solution 1: Make the tree immutable and pre-compute the maxima as you build it.

If we implement our MaxTree to be immutable we can keep track of the current maximum as we build it. Since it's immutable, we can be sure __the cached maximum will never be outdated__. In general, making your data structures immutable can give you a lot of goodies (but that's a story for another day). So we can start off with something like this:
```java
public class ImmutableMaxTree implements MaxTree {
    private final double max; // this is we're we'll cache the maximum of the tree's children. Notice how it's final, since it will never change (immutability constraint).
    private final List<ImmutableMaxTree> children;
    
    private ImmutableMaxTree(List<ImmutableMaxTree> children) {
        // cache the maximum of children in the max field 
    }
}
```
__Q2 [0.5 marks]__: Finish implementing the constructor `ImmutableMaxTree(List<ImmutableMaxTree> children)`.

Run `Main.java`. You should see `Immutable Max Tree test passed".

## Non-recursive Solution 2: Make the (mutable) tree reactive and backpropagate updates.

Now you might be thinking _Sweet! But this only works if I don't have any mutable leaves... What if I need mutability? Can I do it without recursion?_. Well the answer is, fortunately, yes. There are a few algorithms in general that allow you to traverse/process the entire tree without using recursion. However a lot of them end up __visiting all the nodes__ which is a huge waste for trees with a large breadth. Look at our initial tree again below. Let's say we wanted to change leaf __3 to 100__. 

![tree1](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab4/tree1.png "A Max Tree")

Notice how after updating 3 to 100, we don't need to revisit the 99-branch on the right because we've already cached that value. In other words, we __only need update the parent nodes__ (see figure below). 
![tree2](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab4/tree2.png "Backprop algorithm")

This is called _backpropagation_, we propagate all changes back to the root of the tree. To implement this we'll start off by creating the `ReactiveMaxTree` abstract class (we'll need a mutable leaf subclass):

```java
public abstract class ReactiveMaxTree implements MaxTree {
    public abstract boolean hasParent(); // Do we have a parent to go to?
    public abstract ReactiveMaxTree getParent(); // Parent reference
    public abstract void setParent(ReactiveMaxTree parent); // internal method used to set parent
    public abstract void triggerUpdate(); // triggers backpropagation
    public abstract void computeMax(); // tell the tree to recompute the max, updating the cached value
}
```

Most of these methods have been implemented for you in the `ReactiveMaxTreeImpl.java` implementation class. You'll only need to implement the constructor `ReactiveMaxTreeImpl()`, `computeMax()` and the utility function `propagateUpdates(ReactiveMaxTree tree)` in `ReactiveMaxTreeUtils.java` (because backpropagation is used by both the leaf and tree classes).

__Q3 [0.25 marks]__: Finish implementing the constructor `ReactiveMaxTreeImpl(List<ReactiveMaxTree> children)`. You should set each child's parent to the current tree (more hints in the code).

__Q4 [0.25 marks]__: Implement the `computeMax()` method.

__Q5 [0.5 marks]__: Finally, implement backpropagation. This should be done in the `propagateUpdates(ReactiveMaxTree tree)` function in `ReactiveUtils.java` (more hints in the code).

Run `Main.java`. You should see all the "Reactive Max Tree" tests passed.

## _Is backpropagation really worth it compared to other non-recursive algorithms?_

If the other algorithms revisit __all of the nodes__ then the answer is yes! In `Main.java` try uncommenting the last line in the following method to see a nice demo.
```java
    private static void runTests() {
        immutableMaxTreeTest();
        reactiveMaxTreeTest();
        // Try uncommenting and running the last line after you've passed all of the tests :-)
//        showDeepTree();
    }
```
The tree's height is 20000 so it will take some time to build. But notice how after updating a node the maximum is recomputed instantly!
