# Lab 1 Challenge

We want to generalise binary operations to operate on lists (we might want to add all of the elements in a list or compute its minimum/maximum). To do this, we can abstract the notion of a binary operator with the BinaryOperator interface and the operate() functions:

```java
    public interface BinaryOperator {
        // operator definition
        double op(double operand1, double operand2);

        // operator identity. identity() must be such that:
        // op(value, identity()) == op(identity(), value) == value
        double identity();
    }

    /**
     * Applies the supplied operator to the entire list effectively reducing it to a single value.
     * If the list supplied list is empty, the operator's identity MUST BE returned.
     */    
    private static double operate(BinaryOperator operator, List<Double> values) {
        throw new IllegalStateException("You haven't implemented me yet!"); // delete this line when you begin
    }

    /**
     * Applies the supplied operator to the entire list effectively reducing it to a single value.
     * If the list supplied list is empty, the operator's identity MUST BE returned.
     */
    private static double operateRecursive(BinaryOperator operator, List<Double> values) {
        // HINT: List::subList() is your friend :-)
        throw new IllegalStateException("You haven't implemented me yet!"); // delete this line when you begin
    }
```

Here are some examples of operators:
```java
    /**
     * Addition
     */
    private static BinaryOperator PLUS = new BinaryOperator() {
        @Override
        public double op(double operand1, double operand2) {
            return operand1 + operand2;
        }

        @Override
        public double identity() {
            return 0;
        }
    };

    /**
     * Multiplication
     */
    private static BinaryOperator TIMES = new BinaryOperator() {
        @Override
        public double op(double operand1, double operand2) {
            return operand1 * operand2;
        }

        @Override
        public double identity() {
            return 1;
        }
    };

    /**
     * Maximum
     */
    private static BinaryOperator MAX = new BinaryOperator() {
        @Override
        public double op(double operand1, double operand2) {
            return Math.max(operand1, operand2);
        }

        @Override
        public double identity() {
            return Integer.MIN_VALUE;
        }
    };
```

With this we can do things like
```java
        List<Double> list = Arrays.asList(1.0, 2.0, 3.0, 4.0);
        double sum = operate(PLUS, list); // compute the sum of a list (this should return 10)
        double product = operate(TIMES, list); // compute the product of a list (this should return 24)
        double max = operate(MAX, list); // compute the maximum of a list (this should return 4)
```

## Your task (2 marks):
Implement the `operate()` function with a for-loop and `operateRecursive()` with recursion. You can download the code [here](https://gitlab.cecs.anu.edu.au/u5786025/comp2100-challenges/raw/master/lab1/Operations.java).

__Note: Undergraduate students need only implement _ONE_ of the functions correctly whereas prostgraduate students _MUST_ implement both.__
## To think about (after the lab):
Think about both implementations and the pros/cons of each of them.

