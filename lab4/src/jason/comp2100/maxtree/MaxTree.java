package jason.comp2100.maxtree;

import java.util.List;

public interface MaxTree {
    double max();
    List<MaxTree> getChildren();
    boolean isLeaf();
}
