package jason.comp2100.maxtree.reactive;

import jason.comp2100.maxtree.MaxTree;

import java.util.Collections;
import java.util.List;

public class ReactiveLeaf extends ReactiveMaxTree {
    private ReactiveMaxTree parent;
    private double max;

    public ReactiveLeaf(double max) {
        this.max = max;
    }

    public void setMax(double max) {
        this.max = max;
        triggerUpdate();
    }

    @Override
    public double max() {
        return max;
    }

    @Override
    public List<MaxTree> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public ReactiveMaxTree getParent() {
        return parent;
    }

    @Override
    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public void setParent(ReactiveMaxTree parent) {
        if (this.parent != null) {
            throw new IllegalStateException("Tree already has a parent!");
        }
        this.parent = parent;
    }

    @Override
    public void triggerUpdate() {
        ReactiveUtils.propagateUpdates(this);
    }

    @Override
    public void computeMax() {}
}
