package jason.comp2100.maxtree.reactive;

import jason.comp2100.maxtree.MaxTree;

import java.util.Arrays;
import java.util.List;

// Abstract class for reactive implementation
public abstract class ReactiveMaxTree implements MaxTree {
    public abstract boolean hasParent();
    public abstract ReactiveMaxTree getParent();
    public abstract void setParent(ReactiveMaxTree parent);
    public abstract void triggerUpdate();
    public abstract void computeMax();

    public static ReactiveLeaf leaf(double value) {
        return new ReactiveLeaf(value);
    }

    public static ReactiveMaxTree makeTree(List<ReactiveMaxTree> children) {
        return new ReactiveMaxTreeImpl(children);
    }

    public static ReactiveMaxTree makeTree(ReactiveMaxTree... children) {
        return makeTree(Arrays.asList(children));
    }
}
