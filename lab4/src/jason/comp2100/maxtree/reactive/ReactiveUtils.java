package jason.comp2100.maxtree.reactive;

public class ReactiveUtils {
    static void propagateUpdates(ReactiveMaxTree tree) {
        ReactiveMaxTree currentTree = tree;
        // Q5 Your code HERE:
        // You should propagate the update from the current node all the way to the parent node. This can be done
        // using the following pseudo-algorithm:

        // 1: while currentTree has a parent
        // 2:     TELL currentTree's parent to update its max value
        // 3:     set currentTree to its parent (this is how the propagation happens)
    }
}
