package jason.comp2100.maxtree.reactive;

import jason.comp2100.maxtree.MaxTree;
import jason.comp2100.maxtree.MaxTreeUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by jason on 26/04/17.
 */
public class ReactiveMaxTreeImpl extends ReactiveMaxTree {
    private ReactiveMaxTree parent;
    private List<ReactiveMaxTree> children;
    private double max;

    ReactiveMaxTreeImpl(List<ReactiveMaxTree> children) {
        MaxTreeUtils.ensureChildrenExist(children);
        this.children = children;
        for (ReactiveMaxTree child : children) {
            // Q3 : Your code HERE: you must set the current tree as the parent of all children
        }
        computeMax();
    }

    @Override
    public double max() {
        return max;
    }

    @Override
    public List<MaxTree> getChildren() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public boolean isLeaf() {
        return children.isEmpty();
    }

    @Override
    public ReactiveMaxTree getParent() {
        return parent;
    }

    @Override
    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public void setParent(ReactiveMaxTree tree) {
        if (this.parent != null) {
            throw new IllegalStateException("Tree already has a parent!");
        }

        this.parent = tree;
    }

    public void triggerUpdate() {
        ReactiveUtils.propagateUpdates(this);
    }

    @Override
    public void computeMax() {
        // Q4 YOUR CODE HERE: Update the following field to store the maximum of all the
        // child nodes (use the method you implemented in Q1)
        this.max = 0;
    }
}
