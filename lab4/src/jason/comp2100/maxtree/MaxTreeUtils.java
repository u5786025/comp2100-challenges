package jason.comp2100.maxtree;

import java.util.List;

public class MaxTreeUtils {
    /**
     * Computes the maximum of all provided children
     */
    public static double max(final List<? extends MaxTree> children) {
        double max = Double.NEGATIVE_INFINITY;

        for (MaxTree child : children) {
            // Q1: Your code HERE: you should iterate through all the children and return the maximum
        }

        return max;
    }

    public static void ensureChildrenExist(List<? extends MaxTree> children) {
        if (children.isEmpty()) {
            throw new IllegalArgumentException("A Non-leaf Tree must have at least one child!");
        }
    }
}
