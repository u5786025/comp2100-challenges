package jason.comp2100.maxtree.immutable;

import jason.comp2100.maxtree.MaxTree;
import jason.comp2100.maxtree.MaxTreeUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Immutable implementation of a Max-Tree
 */
public class ImmutableMaxTree implements MaxTree {
    private final double max;
    private final List<ImmutableMaxTree> children;

    private ImmutableMaxTree(List<ImmutableMaxTree> children) {
        MaxTreeUtils.ensureChildrenExist(children);
        this.children = children;
        // Q2: YOUR CODE HERE! You should change the following line of code and store the maximum of all the
        // child nodes (use the method you implemented in Q1)
        this.max = 0;
    }

    private ImmutableMaxTree(double value) {
        this.max = value;
        this.children = Collections.emptyList();
    }

    /**
     * Creates a new MaxTree with the supplied children
     */
    public static ImmutableMaxTree makeTree(List<ImmutableMaxTree> children) {
        return new ImmutableMaxTree(children);
    }

    public static ImmutableMaxTree makeTree(ImmutableMaxTree... children) {
        return makeTree(Arrays.asList(children));
    }

    /**
     * Creates a new leaf
     */
    public static ImmutableMaxTree leaf(final double value) {
        return new ImmutableMaxTree(value);
    }

    @Override
    public double max() {
        return max;
    }

    @Override
    public List<MaxTree> getChildren() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public boolean isLeaf() {
        return children.isEmpty();
    }
}
