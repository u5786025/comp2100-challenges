package jason.comp2100.test;

import jason.comp2100.maxtree.immutable.ImmutableMaxTree;
import jason.comp2100.maxtree.reactive.ReactiveLeaf;
import jason.comp2100.maxtree.reactive.ReactiveMaxTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jason on 26/04/17.
 */
public class Main {
    public static void main(String[] args) {
        runTests();
    }

    private static void runTests() {
        immutableMaxTreeTest();
        reactiveMaxTreeTest();
        // Try uncommenting and running the last line after you've passed all of the tests :-)
//        showDeepTree();
    }

    private static void showDeepTree() {
        ReactiveLeaf shallow = ReactiveMaxTree.leaf(-1);
        ReactiveMaxTree deep = ReactiveMaxTree.leaf(0);
        System.out.println("Building deep tree... (this might take a while...)");
        List<ReactiveMaxTree> children = new ArrayList<>();
        for (int i = 0; i < 20000; i++) {
            children.clear();
            for (int j = 0; j < i; j++) {
                children.add(ReactiveMaxTree.leaf(i * j + 1));
            }
            if (!children.isEmpty()) {
                deep = ReactiveMaxTree.makeTree(deep, ReactiveMaxTree.makeTree(children));
            }
        }
        deep = ReactiveMaxTree.makeTree(shallow, deep);
        System.out.println("Finished building tree. Computing max...");
        System.out.println(deep.max());

        double hugeNumber = 1E10;
        System.out.printf("Changing shallow node to %e. Computing max...%n", hugeNumber);
        shallow.setMax(hugeNumber);
        System.out.println(deep.max());
        System.out.println("Wow! That was super fast, wasn't it?");
    }

    private static void reactiveMaxTreeTest() {
        ReactiveLeaf x = ReactiveMaxTree.leaf(54);
        ReactiveLeaf y = ReactiveMaxTree.leaf(123);
        ReactiveLeaf z = ReactiveMaxTree.leaf(-99);

        ReactiveMaxTree maxTree = ReactiveMaxTree.makeTree(
                ReactiveMaxTree.makeTree(
                        ReactiveMaxTree.leaf(2),
                        z
                ),
                ReactiveMaxTree.makeTree(
                        x,
                        ReactiveMaxTree.makeTree(
                                y,
                                ReactiveMaxTree.leaf(-256)
                        )
                )
        );
        assertEquals("Reactive MaxTree [1]", maxTree.max(), 123);
        y.setMax(-2);
        assertEquals("Reactive MaxTree [2]", maxTree.max(), 54);
        z.setMax(666);
        assertEquals("Reactive MaxTree [3]", maxTree.max(), 666);
    }

    private static void immutableMaxTreeTest() {
        ImmutableMaxTree maxTree = ImmutableMaxTree.makeTree(
                ImmutableMaxTree.makeTree(
                        ImmutableMaxTree.leaf(2),
                        ImmutableMaxTree.leaf(-113)
                ),
                ImmutableMaxTree.makeTree(
                        ImmutableMaxTree.leaf(54),
                        ImmutableMaxTree.makeTree(
                                ImmutableMaxTree.leaf(123),
                                ImmutableMaxTree.leaf(-256)
                        )
                )
        );

        assertEquals("Immutable MaxTree", maxTree.max(), 123);
    }

    private static void assertEquals(String name, double found, double expected) {
        if (found != expected) {
            System.err.printf("%s test FAILED! Expected %f, found %f instead!%n", name, expected, found);
            return;
        }
        System.out.printf("%s test PASSED ;-)%n", name);
    }
}
